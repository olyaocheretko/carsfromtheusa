/*$(document).ready(function(){*/
  
  $('.products-body').slick({
    arrows: false,
  	slidesToShow: 3,
  	dots: true,
  	autoplay: true,
  	autoplaySpeed: 3500,
  	responsive: [
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 2,
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	      }
	    }
    ]
  });

/*});*/



